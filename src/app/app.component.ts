import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Angular';
  users: { name: string, title: string }[] = [
    { name: 'Yassine', title: 'Coordinateur' },
    { name: 'Patrick', title: 'Formateur' },
    { name: 'Loic', title: 'Formateur' },
    { name: 'Thomas', title: 'Destructeur de one drive' },
    { name: 'Pierre', title: 'Formateur' },
  ];
}
