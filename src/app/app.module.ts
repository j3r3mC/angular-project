import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { TemplateModule } from './template/template.module';
import { MessagingModule } from './messaging/messaging.module';

import { AppComponent } from './app.component';
import { FooterComponent } from './template/footer/footer.component';
import { HeaderComponent } from './template/header/header.component';
import { ContentComponent } from './template/content/content.component';
import { NavigationComponent } from './template/navigation/navigation.component';

import { NotificationComponent } from './messaging/notification/notification.component';
import { InboxComponent } from './messaging/inbox/inbox.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NbThemeModule, NbLayoutModule, NbSidebarModule, NbMenuModule, NbCardModule, NbListModule, NbButtonModule, NbBadgeModule, NbChatModule, NbUserModule} from '@nebular/theme';
import { NbEvaIconsModule } from '@nebular/eva-icons';
import { AppRoutingModule } from './app-routing.module';

@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    HeaderComponent,
    ContentComponent,
    NavigationComponent,
    InboxComponent,
    NotificationComponent
  ],
  imports: [
    BrowserModule,
    TemplateModule,
    MessagingModule,
    BrowserAnimationsModule,
    NbThemeModule.forRoot({ name: 'dark' }),
    NbLayoutModule,
    NbEvaIconsModule,
    NbSidebarModule.forRoot(),
    NbMenuModule.forRoot(),
    NbCardModule,
    NbListModule,
    NbButtonModule,
    NbBadgeModule,
    NbChatModule.forRoot({ messageGoogleMapKey: 'MAP_KEY' }),
    AppRoutingModule,
    NbUserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
