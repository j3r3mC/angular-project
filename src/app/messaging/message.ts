export interface Message {
    id:number,
    sender : string,
    receiver : string,
    read: boolean,
    reply: boolean,
    content: string,
    avatar?: string,
    color?: string
}
