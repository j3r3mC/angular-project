import { Component, OnInit } from '@angular/core';
import { MessagingService } from '../messaging.service';
import { Message } from '../message';


@Component({
  selector: 'app-inbox',
  templateUrl: './inbox.component.html',
  styleUrls: ['./inbox.component.css']
})

// Classe permettant de gerer la partie de la boite de dialogue
export class InboxComponent implements OnInit {

  constructor(private service: MessagingService) { }

  messages:Message[] = [];

  
  ngOnInit(): void 
  {
    /**
     * @getMessages methode permettant de defiir une culeur au background du essage selon si il est lu ou non
     * à la récupératio des messages
     */
    this.service.getMessages().subscribe((request) =>
    {
      this.messages = request;
      this.messages.forEach(message => 
        {
        if(!message.read)
        {
          message.color = "#9c1e20";
        }
        else
        {
          message.color = "#00964e";
        }
        
      })
    })
  }
  //J'implémente la fonction sendMessage qui servira a appeller la methode 
  //addMessage située dans le messaging.service.ts
  sendMessage(event:any)
  {

      console.log(event.message);
      //configuration des messages envoyés
      let newMessage: Message =
        {
        "id":0,
        "sender":"J3r3mC",
        "content": event.message,
        "receiver":"Loic",
        "read":true,
        "reply":false
        }
        this.service.addMessages(newMessage).subscribe();    
  }
   
  //J'implémente la fonction delMessage qui servira a appeller la methode 
  //deleteMessage située dans le messaging.service.ts
  delMessage(id:number)
  {  

    this.service.deleteMessage(id).subscribe();
     
  }
   
}
