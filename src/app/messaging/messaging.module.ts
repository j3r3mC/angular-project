import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';

import { WritingComponent } from './writing/writing.component';



@NgModule({
  declarations: [
    WritingComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule
  ]
})
export class MessagingModule { }
