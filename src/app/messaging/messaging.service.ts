import { Injectable, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Message } from './message';
import { Observable } from 'rxjs';
import { delay, map, repeat } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class MessagingService {

  maxNotifs: number = 5
  @Input() maxMessages: number = 0;
  count: number = 0;

  constructor(private http: HttpClient) { }

  private messages: Message[] = [];

  // Méthode qui permet dde compter le nombre de message non lu
  getCountUnreadMessage(): Observable<number>
  {
    return this.http.get<Message[]>("http://localhost:4477/messages")
    .pipe
    (
      map(messages =>{
        return messages.filter(m => !m.read).length;       
      }),
      
    )
  }
  // Méthode qui permet de récupérer les messages dans le backend puis le .pipe permet de rajouter un delay de 1000 ms
  //avant de repéter l'opération pour avoir un refresh de l'affichage des messages
  getMessages(): Observable<Message[]>{
    return this.http.get<Message[]>("http://localhost:4477/messages").pipe(
      delay(1000),
      repeat()
    );
     }
  
  //Méthode permettant d'envoyer une requete perettant d'ajouté un message dans le backend
  
  addMessages(request:Message){

    return this.http.post<Message>("http://localhost:4477/messages", request);

  }

  //Méthode permettant d'envoyer une requete perettant d'ajouté un message dans le backend
  deleteMessage(id: number){

    return  this.http.delete<Message>(`http://localhost:4477/messages/${id}`);
  }

}
