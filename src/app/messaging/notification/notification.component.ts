import { Component, Input, Injectable, OnInit } from '@angular/core';
import { MessagingService } from '../messaging.service';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.css']
})

@Injectable({
  providedIn: 'root'
})

// Classe permetant d'afficher le nombre de messages non lu
export class NotificationComponent implements OnInit {

  @Input() maxMessages: number = 0;

  count: number = 0;

  constructor(private service: MessagingService) { }
  
  ngOnInit(): void {
    this.service.getCountUnreadMessage().subscribe((number) =>{
      this.count = number;
      
      //condition permettant de limiter le nombre maximum de message non lu
      if(this.count >= this.maxMessages) this.count = this.maxMessages;
    })
  }

}
