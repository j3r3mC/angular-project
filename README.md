# Frontend-angular de la messagerie

Objectif:

- Approfondir angular
- Créer un frontend pour une messagerie
- travailler le typage ainsi que les observable
- découverte des .pipe, prommesse ainsi que behavorSubject


Techno utilisé:

- Angular

- Nebular


Fonctionnement:

- commande: git clone https://gitlab.com/dwwm_bze_05/angular-ionic/cj-front-angular.git
- commande: npm install
- commande: ng serve
- Dans le navigateur se rendre sur localhost:4200
